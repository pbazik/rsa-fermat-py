from Crypto.PublicKey import RSA
from math_alg import fermat, modinv


def main():
    N = RSA.importKey(open('keys/01-pub.pem', 'r').read()).n
    p, q = fermat(N)
    phi = (p - 1) * (q - 1)
    e = 65537
    private_key = RSA.construct((N, e, modinv(e, phi), p, q)).exportKey()
    open('01-priv.pem', 'w').write(private_key.decode("utf-8"))


if __name__ == "__main__":
    main()
