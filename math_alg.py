def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    g, y, x = egcd(b % a, a)
    return (g, x - (b // a) * y, y)


def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modinv doesn\'t exists..')
    return x % m


def isqrt(n):
    x, y = n, (n + 1)//2
    while y < x:
        x = y
        y = (x + n // x) // 2
    return x


def fermat(n):
    a = b = isqrt(n)
    b2 = (a * a) - n

    while (b * b) != b2:
        a += 1
        b2 = (a * a) - n
        b = isqrt(b2)

    assert n == (a + b) * (a - b)
    return int(a + b), int(a - b)
